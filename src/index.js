import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import ToTop from './pages/components/totop.js'

ReactDOM.render(<BrowserRouter>
    <ToTop>
    <App />
    </ToTop>
    </BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
