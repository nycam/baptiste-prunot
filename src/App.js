import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom'

/**
 * Pages
 */
import Home from './pages/Home.js'
import TDScanning from './pages/projects/3DScanning.js'
import Whis from './pages/projects/Whis.js'
import Jiren from './pages/projects/Jiren.js'
import IntegrationUTT from './pages/projects/integrationUTT.js'
import Ginyu from './pages/projects/ginyu.js'
import SpaceConquest from './pages/projects/spaceconquest.js'

/**
 * Components
 */
import Footer from './pages/layouts/footer.js'

class App extends Component {

    constructor(props) {
        super(props)
        this.handleSelectionNavbar = this.handleSelectionNavbar.bind(this)
    }

    render() {
        return (
            <div className="uk-container-expand uk-background-muted">
                <Switch>
                    <Route exact path="/" render={() => <Home onClick={(pageName) => this.handleSelectionNavbar(pageName)}/> }/>
                    <Route path="/3dscanning" component={TDScanning } />
                    <Route path="/whis" component={Whis} />
                    <Route path="/jiren" component={Jiren }/>
                    <Route path="/integration" component={IntegrationUTT }/> 
                    <Route path="/ginyu" component={Ginyu }/>
                    <Route path="/spaceconquest" component={SpaceConquest }/>
                </Switch>
                <Footer />
            </div>
            );
}

handleSelectionNavbar(pageIndex) {
    this.setState({
        current: pageIndex
    })
    window.scrollTo(0, 0)
}
}

export default App;
