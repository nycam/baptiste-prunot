import React, {Component} from 'react'
class Navbar extends Component{
	constructor(props) {
		super(props)
		this.selectTab  = this.selectTab.bind(this)
	}
	selectTab(tab) {
		this.props.onClick(tab);
	}
	render() {
		return (
			<nav className="uk-navbar-container" uk-navbar="true">
				<div className="uk-navbar-left">
					<ul className="uk-navbar-nav">
						<li className="active">
							
							{/*passing 1 to the function allows for the dashboard to know which tab has been clicked :)*/}
							
							<a href="#" onClick={() => {this.selectTab('home')}}>Home</a>
						</li>
						<li>
							<a href="#">Experience</a>
							<div className="uk-navbar-dropdown">
								<ul className="uk-nav uk-navbar-dropdown-nav">
                                    <li> <a onClick={() => { this.selectTab("3dscanning") }}>3D scanning</a> </li>
                                    <li><a onClick={() => { this.selectTab("whis") }}>Whis</a></li>
                                    <li><a onClick={() => { this.selectTab("jiren") }}>Jiren</a></li>
                                    <li><a onClick={() => { this.selectTab('integration') }}>Integration UTT</a></li>
								</ul>
							</div>
						</li>
						<li><a href="#">Resume</a></li>
					</ul>
				</div>
			</nav>
		)
	}
}
export default Navbar
