import React from 'react';
import Title from './title.js'

const MainContent =(props) => {
    return (
        <div>
            <Title name={props.title}/>
            <div className="uk-container">
                { props.children }
            </div>
        </div>
        )
}

export default MainContent;
