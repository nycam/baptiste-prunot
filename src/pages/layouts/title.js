import React from 'react';

const Title = (props) => {
    return (
        <div className="uk-tile uk-tile-secondary uk-text-center">
            <h3 className="uk-heading-hero">{ props.name }</h3>
        </div>
    );
}

export default Title;
