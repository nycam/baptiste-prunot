import React from 'react'

const SubSection = (props) => {
    return (
        <div>
            <p className="uk-text-lead">{ props.title }</p>
            { props.children }
        </div> 
    );
}

export default SubSection;
