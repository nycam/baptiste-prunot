import React from 'react'

const TLDR = (props) => {
    return (
        <div className="uk-card uk-card-default uk-width-1-1@s uk-width-1-3@l uk-align-center uk-text-center">
            <div className="uk-card-header">
                <h1 className="uk-card-title" uk-tooltip="Too Long Did not read 😉">TL;DR</h1>
                <p className="uk-text-meta">Project overview</p>
            </div>
            <div className="uk-body">
                <ul className="uk-list uk-list-striped">
                    { 
                    props.keyPoints.map((e, index) => {
                        return <li key={index} className="uk-text-uppercase"> <p className="uk-text-left uk-text-lead">{e}</p></li>
                        })
                    }
                    </ul>
                </div>
            </div>
            );
}

export default TLDR;
