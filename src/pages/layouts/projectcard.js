import React from 'react'
import Button from '../components/button.js'
import {Link} from 'react-router-dom'

const ProjectCard = (props) => {

    let state
    /**
     * if props.dev is empty
     * I give a default value
     */
    if (!props.dev) {
        state = {
            dev: false
        }
    }else{
        state = {
            dev: true
        }
    }

    function renderButtons(props) {
        return !state.dev? buttons(props):<p>Sorry, this page is under construction</p>
    } 

    function buttons(props){
        return (
            <div>
            {props.index?<Link to={props.index}><Button className="uk-button-default" content="Read More"/></Link>:""}
                
                {props.source != null ?
                (<a href={props.source}><Button className="uk-button-secondary" content={"View source"}/></a>):
                ""}
            </div>
            )

}

return (
    <div className="uk-card uk-card-default uk-width-1-1@s uk-width-1-3@l uk-margin uk-margin-right uk-margin-bottom">
        <div className="uk-card-header uk-text-center">
            <h3 className="uk-card-title">{props.name}</h3>
        </div>

        <div className="uk-card-media-top">
            {
            props.image != null ? 
            (<img className="uk-align-center" height="10px" src={props.image} alt="" uk-img="true"/>):
            ""
            }
        </div>

        <div className="uk-card-body uk-text-left">
            {props.children}<span className="uk-text-bold">{ props.hashtags }</span> 
        </div>

        <div className="uk-card-footer uk-text-center">
            {renderButtons(props)}
        </div>
    </div>
    );
}

export default ProjectCard;
