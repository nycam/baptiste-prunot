import React from 'react'

const SmallTitle = (props) => {
    return (
        <div className="uk-tile">
            <h2 className="uk-heading-divider">{props.name}</h2>
        </div>
    );
}

export default SmallTitle;
