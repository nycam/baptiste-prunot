import React from 'react'
import Button from '../components/button.js'
import {Link, Route} from 'react-router-dom'

const Footer = () => {
    return (
        <footer className="uk-tile uk-tile-secondary">
            <div className="uk-text-center">
                <a href="" uk-totop="" uk-scroll=""><p className="uk-text-lead uk-text-uppercase">to top</p></a>
                <div className="uk-margin">
                    {/*Regex to match everything but the root*/} 
                    <Route path={/^\/[a-z]+/} render={() => <Link to="/"><Button className="uk-button-primary " content="back home" /></Link>}/>
                </div>
            </div>
        </footer>
        );
}

export default Footer
