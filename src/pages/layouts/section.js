import React from 'react';

/**
 * title: title of the section
 */
const Section = (props) => {
    return (
        <div className="uk-container uk-background-muted">
            <h4 className="uk-heading-bullet uk-text-lead">{props.title}</h4>
             {props.children}
        </div>
        );
}

export default Section;
