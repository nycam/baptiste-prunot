import React from 'react'


const Image = (props) => <img src={props.src} alt={props.alt} className="uk-align-left uk-width-1-1@s uk-width-1-3@l" uk-img="true" />

export default Image;
