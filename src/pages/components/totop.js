/**
 * The component scrolls the window to
 * top each time it is updated
 */
const ToTop = (props) => {
    return {
        componentDidUpdate: (prevProps) => {
            window.scrollTo(0,0)
        },

        render: () => props.children

    }
}

export default ToTop;
