import React from 'react'

const Button = (props) => <button onClick={props.onClick} className={"uk-button "+ props.className}>{props.content}</button>

export default Button;
