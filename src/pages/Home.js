import React from 'react'

/**
 * Ui components
 */
import Title from './layouts/title.js'
import ProjectCard from '../pages/layouts/projectcard.js'
import SmallTitle from '../pages/layouts/smalltitle.js'

/**
 * Images
 */
import ginyu from '../images/ginyu-menu.png'
import integration from '../images/integration.jpg'
import bob from '../images/bob.png'

const Home = (props) => {
    return (
        <div>
            <Title name="Baptiste Prunot"/>
                <SmallTitle name="Some Projects" />
                <div className="uk-grid-small uk-child-width-expand@s uk-flex-center uk-margin" uk-grid="parallax: 250">

                    <ProjectCard source={"https://github.com/nyc4m/retarded-bob-generator"} name="Retarded Sponge Bob Generator" hashtags="#Go #Meme" image={bob}>
                        <p>Funny project to generate this famous meme</p>
                    </ProjectCard>

                    <ProjectCard source={"https://github.com/ungdev/integration-UTT"} name="Integration-UTT" hashtags="#Php #Laravel #UTT" image={integration} index={"integration"}>
                        <p>A small work I did for the integration website of my school. I have added a challenge system to make the integration week funnier <span role="img" aria-label="smirking face">😏</span>.</p>
                    </ProjectCard>

                    <ProjectCard image={ginyu} source={"https://gitlab.com/nycam/ginyu"}name="Ginyu" hashtags="#Php #Laravel" index={'ginyu'} > 
                        <p>A small website I did for my brother in order to help him manage his treasure hunt.</p>
                    </ProjectCard>               

                    <ProjectCard source={"https://gitlab.com/nycam/Whis"} name="Whis" hashtags="#Php #Symfony" index={'whis'}>
                        <p>One of the project I did for a course inside the UTT. It's a website to help parents to find a nurse.</p>
                    </ProjectCard>

                    <ProjectCard dev={true} name="Jiren" hashtags="#Qt #C++" index="jiren" >
                        <p>Personnal project I did for fun. It's an app to help restaurant to manage their reservations. Far from being finished</p>
                    </ProjectCard>

                    <ProjectCard dev={true} name="3DScanning" hashtags="#C++ #Qt #openCV" index={'3dscanning'} >
                        <p>Project done during my DUT internship, in Thailand <span role="img" aria-label="happy smiley">😃</span></p>
                    </ProjectCard>

                </div>

                <div className="uk-container uk-margin">
                    <p className="uk-text-meta">You may have noticed that some projects name are based on Dragon Ball Z characters. Yep that's right, it's just because I like Dragon Ball Z and I have no inspiration to name my projects. It's just a quicker way to name things when I create a github/gitlab repository.</p>
                </div>
        </div>
        );
}

export default Home;
