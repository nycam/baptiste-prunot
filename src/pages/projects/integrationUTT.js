import React from 'react'

/**
 * UI elements
 */
import Title from '../layouts/title.js'
import SmallTitle from '../layouts/smalltitle.js'
import Section from '../layouts/section.js'
import TLDR from '../layouts/tldr.js'
import Image from '../components/image.js'

/**
 * Images
 */
import challenge_workflow from '../../images/challenges_work_flow.png'
import integration from '../../images/integration.jpg'



const IntegrationUTT = (props) => {
    return (
        <div className="uk-background-muted">
            <Title name={"Integration UTT"}/>
            <SmallTitle name={"Challenges system"} />
            <Section title="Description">
                <Image src={ integration } alt="logo intégration" />
                <p>Like in many french engineer schools, the first week prior to the beginning of the scholar year 
                    is the occasion for the new students to be integrated inside the school by the the other students.
                    For some years now, the new students are grouped in teams and have to pass some challenges (If they are willing to <span role="img" aria-label="wink">😉</span>). 
                    That's where the challenges system comes.
                </p>
            </Section>

            <TLDR keyPoints={
                ["Existing Website", "One feature added", "Framework: Laravel" ]
                }/>

            <Section title="Implementation"> 
                <p>In order to make the points accounting easier, it has been decided to develop a challenges system.
                    The system allows to create challenges specifying:
                </p>
                <ul>
                    <li className="uk-list uk-list-bullet">
                        <li>a name</li>
                        <li>a description</li>
                        <li>a deadline, passing that deadline it is impossible to ask a validation for the challenge</li>
                        <li>an amount of point</li>
                    </li>
                </ul>

                <p>Once a list of challenge has been defined, the teams can ask validation for a particular challenge. Images are more talkative than long sentences, so the typical workflow is described with the image below:</p>
                        

                <img className="uk-align-center" src={challenge_workflow} alt="" uk-img/>

                <p>If the administrator validates the challenge, the team win the points associated, otherwise the team can have as many retry as they wants</p>

    </Section>



</div>
);
}

export default IntegrationUTT;
