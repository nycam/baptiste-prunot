import React from 'react'
import Title from '../layouts/title.js'
import SmallTitle from '../layouts/smalltitle.js'
import Section from '../layouts/section.js'

const SpaceConquest = (props) => {
    return (
        <div>
            <Title name='Space Conquest' />
            <SmallTitle name='A graph based project' />
            <Section title='description'>

            </Section>
        </div>
    );
}

export default SpaceConquest;
