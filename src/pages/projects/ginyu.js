import React from 'react'

/*
 *UI elements
 */
import Title from '../layouts/title.js'
import Section from '../layouts/section.js'
import SmallTitle from '../layouts/smalltitle.js'
import Image from '../components/image.js'
import TLDR from '../layouts/tldr.js'
import SubSection from '../layouts/subsection.js'
import MainContent from '../layouts/maincontent.js'

/**
 * Images
 */
import ginyu_menu from '../../images/ginyu-menu.png'
//import ginyu_teams from '../../images/ginyu-teams.png'

const Ginyu = (props) => {
    return (
        <div>

            <Title name="Ginyu" />
            <MainContent>
                <SmallTitle name="Treasure hunt manager"/>

                <Section title="Description">
                    <Image src={ginyu_menu} alt="screenshot from the dashboard" />
                    <p>My brother wanted to organize a treasure hunt for his birthday, with a background story talking about spying.
                        As his brother I wanted to help him, so I proposed him to build a website to help manage his treasure hunt.
                        That was also a way for me to practice with <span className="uk-text-bold">Laravel</span> and <span className="uk-text-bold">Amazon Web Services</span> </p>
                </Section>

                <Section title="Game description">
                    <ul className="uk-list uk-list-bullet">
                        <li>The game is a succession of checkpoints through a city</li>
                        <li>At each checkpoint, a clue is revealed and the team has to guess where is the next checkpoint</li>
                        <li>if the team doesn't succeed after 5 minutes, an advice is dropped</li>
                        <li>each checkpoint is also the occasion to have a little piece of the story</li>
                    </ul>
                </Section>

                <TLDR keyPoints={[
                    "Website from scratch", "Tool to manage clues and advices", "Responsive website"
                    ]} />

                <Section title="Implementation">
                    <SubSection title="Adding clues">
                        <p>It has been decided very early that the website would be built as a webapp, and used on players smartphones to get information about the treasure hunt, like in which team they are, see clues, get help etc.
                            Since the game is mostly based on clues, I had to create a feature to let my brother add all the clues he wanted to add. The clues were just YouTube videos, to simplify the process, and also save some storage :). </p>
                    </SubSection>

                    <SubSection title="Unlocking clues for player">
                        <p>
                            At first we wanted to track the position of each player, and unlock clues only if they were near the right area to make the story (and the hunt) continue. Unfortunately, for technical details, I could not access the localisation on each devices. <br />
                            <span className="uk-text-bold">Probleme:</span> How to track the players and unlock clues at the right time. <br />
                            <span className="uk-text-bold">Solution:</span> Introduce a 'guide' in each team that can unlock manually the clues at the right time. <br />
                            This role was different from the other player, and the guide as such wasn't consider as a player, more like a role master.
                            Once connected, the guide had just one button to click to unlock the next clue, and if the team needed help he could also tap one button to unlock an advice.
                        </p>
                    </SubSection>
                    <SubSection title="AWS ?">
                        <p>The website was hosted on Amazon Web Service, on the Elastic Beanstalk service. It allowed me to have a plateform automatically scaled on my needs.
                            It was also a good mean to deploy my code easily, with one command line.</p>
                    </SubSection>
                </Section>

            </MainContent> 
        </div>
);
}

export default Ginyu;
