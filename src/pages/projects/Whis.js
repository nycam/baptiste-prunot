import React from 'react';

/**
 * Ui element
 */
import SmallTitle from '../layouts/smalltitle.js'
import MainContent from '../layouts/maincontent.js'
import Section from '../layouts/section.js'
import SubSection from '../layouts/subsection.js'
import TLDR from '../layouts/tldr.js'

const Whis = (props) => {
    return (
            <MainContent title="Whis">
                <SmallTitle name="Website to help parents find a nurse"/>

                <Section title="Description">
                    This website was a pure school project. The idea was to build an online platform to learn how to combine php, html, javascript and SQL to make a useful application for a specified public.
                    The public concerned here was parents. The main goal was to make their life easier by providing them a tool to seach a nurse corresponding to multiple criterias such as:
                    <ul className="uk-list uk-list-bullet">
                        <li>Localisation</li>
                        <li>experience</li>
                        <li>available time</li>
                        <li>languages spoken</li>
                    </ul>
                    <p>A rating system was also involved.</p>
                </Section>

                <TLDR keyPoints={
                    ["Website from scratch", "Symfony autonomous learning", "Team work", "Backend"]
                    }/>

                <Section title="Implementation">
                    <SubSection title="Backend: Symfony">
                        <p>This project was designed for very beginner developers, who were just started using php and as such <span className="uk-text-bold">no framework was required</span> to achieve this project. However, I already had a small webdevelopper exerience, and I already knew how to use Php/Html/Javascript. I really wanted to learn something with this project, and as such I decided to use the framework <span className="uk-text-bold">Symfony</span>.</p>
                        <p>In this project, symfony allowed me to setup a communication between my database and the website, a routing system, and an authentication system.</p>
                    </SubSection>
                    <SubSection title="Frontend: Twig">
                        <p>To be honest, writing pure html is p*** i the a** :). Symfony comes with the temple engine Twig, so this is what I used for this project.</p>
                    </SubSection>
                </Section>
            </MainContent>

        );
}

export default Whis;
